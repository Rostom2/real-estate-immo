var app = angular.module('schambi', []);


/////////////////////////////////// Upload image annonce Ros /////////////////////////////////////
var avatar = ""
	//Upload avatar et recuperation url image
	  document.getElementById("upload_widget_opener").addEventListener("click", function() {
	    cloudinary.openUploadWidget({ cloud_name: 'difxdkybn', upload_preset: 'gzn8kjgn'}, 
	      function(error, result) { 
	    	if(error) console.log(error)
	    	avatar = result[0]["secure_url"]
	    	});
	  }, false);

///////////////////////////////////    Controller ROSTOM   ////////////////////////////////////////

app.controller('schambi', function($scope, $http) {

	$http({
		method : 'GET',
		url : 'http://localhost:8090/api/user/me'
	}).then(function(response) {
		$scope.status = response.status;
		$scope.user = response.data.objet;
		//$scope.user = test;
		console.log(response);
	}, function(response) {
		$scope.data = response.data || 'Request failed';
		$scope.status = response.status;
	});
	
	$scope.submit = function () {
		
			var data = {}
			data['login'] = document.getElementById('login').value;
			data['nom'] = document.getElementById('nom').value;
			data['prenom'] = document.getElementById('prenom').value;
			data['email'] = document.getElementById('email').value;
			data['avatar'] = document.getElementById('photoprofil').value;
	
			
			var config = {headers : {'Content-Type': 'application/json;'} }  	
		    
    	
    		$http({method: 'PUT', url: 'http://localhost:8090/api/user/modify' ,data,config}).
	        then(function(data, status, headers, config) {
	        	 $scope.PostDataResponse = data.data.log;
	        	 //$scope.PostDataResponse=data
	        	 
	        }, function(response) {
		          $scope.PostDataResponse = response.data || 'Request failed';
		          $scope.status = response.status;
	        });
		
	}
});



///////////////////////////////////    Controller NICO   ////////////////////////////////////////


	app.controller('getallmsg', function ($scope,$http) {
			$http({method: 'GET', url: 'http://localhost:8090/api/message/inbox'}).
		    then(function(response) {
		    	$scope.status = response.status;
		    	$scope.messages = response.data.objet;
		    	console.log(response)
		    }, 
		    function(response) {
		    	$scope.data = response.data || 'Request failed';
		    	$scope.status = response.status;
		    });	
	});
	
	
	
	app.controller('postmsg',function ($scope,$http) {
		$scope.send = function () {
			           // use $.param jQuery function to serialize data from JSON 
		var data = {}
		var user = {}
		user['email']=$scope.destinataire
		data['title'] 	= $scope.objet;
		data['dest'] = user;
		data['content'] = $scope.message;
		var config = {headers : {'Content-Type': 'application/json;'} }  	
				   
		$http({method: 'POST', url: 'http://localhost:8090/api/message/send' ,data,config}).
			then(function(data, status, headers, config) {
				$scope.PostDataResponse = data.data.log;
				console.log(data)
			}, 
			function(response) {
				$scope.PostDataResponse = response.data || 'Request failed';
				$scope.status = response.status;
			});	
		}
	});
	
	
	
	
	///////////////////////////////////    Controller PAULINE   ////////////////////////////////////////
	
	
app.controller('creAnnonceCtrl', function ($scope,$http) {
	
		$scope.submit = function() {
				console.log("dans fonction submit annnonce")
			var data = {}
			data['titre'] = $scope.titre;
			data['type'] = $scope.type;
			data['location'] = $scope.location;
			data['localisation'] = $scope.localisation;
			data['surface'] = $scope.surface;
			data['budget'] = $scope.budget;
			data['pieces'] = $scope.pieces;
			data['meubler'] = $scope.meubler;
			data['contenu'] = $scope.contenu;
			if (avatar === ""){
				data['img'] = 'https://res.cloudinary.com/difxdkybn/image/upload/v1528447276/disney.jpg';		
			}else{
				data['img'] = avatar
			}
		
			
			console.log("DATA IS : ")
			console.log(data)
			var config = {headers : {'Content-Type': 'application/json;'}}
			
			
			$http({
				method:'POST',
				url:'http://localhost:8090/api/annonce/create',
				data,
				config
			}).
			then(function(data, status, headers, config) {
				console.log(data);
				$scope.PostDataResponse = data.data;
			}, function(response) {
				$scope.PostDataResponse = response.data || 'Request failed';
				$scope.status = response.status;
			});
		}		
	});