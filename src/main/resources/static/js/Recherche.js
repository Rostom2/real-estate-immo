var app = angular.module('Recherche', ["ngRoute"]);


app.config(function($routeProvider) {
	
    $routeProvider
    .when("/", {
        templateUrl : "rechercheBis.html",
        controller : "RechercheFiltreCtrl"
    })
    .when("/annonce", {
        templateUrl : "annonce.html",
        controller : "annonceCtrl"
    })
    .when("/proprio", {
        templateUrl : "user.html",
        controller : "userCtrl"
    })
});
	
	app.controller("mainCtrl", function ($scope, $rootScope) {
		
		$scope.submit = function() {
			$rootScope.login =  $scope.name
		}
	})
	
	app.controller("annonceCtrl", function ($scope) {
		
	})
	
	app.controller("userCtrl", function ($scope) {
		
	})	

	
	
	
///////////////////////////////////    Controller ROSTOM   ////////////////////////////////////////
	
	
		app.controller('RechercheFiltreCtrl', function($scope,$http,$rootScope) {
			$http({method:'GET', url:'http://localhost:8090/api/annonce/searchAll'}).
			then(function(response) {
				//liste Type
				$scope.lisType =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.type)					
					console.log(obj.type)
				})
				
				$scope.lisType = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				
				//liste localisation
				$scope.lisLocalisation =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.localisation)					
					console.log(obj.localisation)
				})
				
				$scope.lisLocalisation = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				//liste location
				$scope.lisLocation =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.location)					
					console.log(obj.location)
				})
				
				$scope.lisLocation = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				//liste pieces
				$scope.lisPieces =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.pieces)					
					console.log(obj.pieces)
				})
				
				$scope.lisPieces = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				
				//liste budget
				$scope.lisBudget =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.budget)					
					console.log(obj.budget)
				})
				
				$scope.lisBudget = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				//liste meubler
				$scope.lisMeubler =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.meubler)					
					console.log(obj.meubler)
				})
				
				$scope.lisMeubler = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				//liste surface
				$scope.lisSurface =  []
				
				var arrObj = response.data.objet
				
				var list = []
				
				arrObj.forEach(function(obj) {
					
					list.push(obj.surface)					
					console.log(obj.surface)
				})
				
				$scope.lisSurface = list.filter(function(value, index, self) { 
				    return self.indexOf(value) === index;
				})
				
				
				//console.log(response.data.objet[0].type)
				$scope.status = response.status;
				$scope.annonces = response.data.objet;
			}, function (response) {
				$scope.data = response.data || 'Request failed';
			    $scope.status = response.status;
			}
			
			);
			
			$scope.AfficheAnnonce = function(annonce){
				console.log("je suis dedans!!")
				console.log(annonce.id)
				console.log(annonce.titre)
				console.log(annonce.localisation)
				$rootScope.annonce = annonce;
			};
		})