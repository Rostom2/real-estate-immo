package com.example.demo.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/*import com.example.demo.model.Annonce;
import com.example.demo.model.Comment;
import com.example.demo.model.Message;*/
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="users")
@EntityListeners(AuditingEntityListener.class)
@DynamicInsert
public class User implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(unique=true)
	private String login;
	
	
	private String password;
	
	@Column(unique=true)
	private String email;
	
	@ColumnDefault("'avatar par défaut'")
	private String avatar;
	
	
	private String nom;
	
	
	private String prenom;
	
	@ColumnDefault("0")
	private int status = 0;
	/*
	
	
	
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="exp")
	private Collection<Message> messagesExp;
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="dest")
	private Collection<Message> messagesDest;*/
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="user")
	private Collection<Annonce> annonces;
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="user")
	private Collection<Comment> comments;
	
	/* Getters et Setters */
	////////////////////////////////////////////////////////////
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "avatar : " + avatar;
	}
	
}
