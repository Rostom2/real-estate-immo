
 package com.example.demo.model;

 import java.io.Serializable;
 import java.util.Date;
 import javax.persistence.Column;
 import javax.persistence.Entity;
 import javax.persistence.EntityListeners;
 import javax.persistence.GeneratedValue;
 import javax.persistence.GenerationType;
 import javax.persistence.Id;
 import javax.persistence.JoinColumn;
 import javax.persistence.ManyToOne;
 import javax.persistence.Table;
 import javax.persistence.Temporal;
 import javax.persistence.TemporalType;
 import javax.validation.constraints.NotBlank;
 import org.springframework.data.annotation.CreatedDate;
 import org.springframework.data.annotation.LastModifiedDate;
 import org.springframework.data.jpa.domain.support.AuditingEntityListener;
 import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

 @Entity
 @Table(name="comments")
 @EntityListeners(AuditingEntityListener.class)
 @JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
         allowGetters = true)


 public class Comment {

 	@Id
 	@GeneratedValue(strategy=GenerationType.IDENTITY)
 	private long id;
 	

 	@NotBlank

 	private String title;
 	
 	@NotBlank
 	private String content;
 	

 	@ManyToOne
 	@JoinColumn(name="id_user")
 	private User user1;
     
 	@ManyToOne
 	@JoinColumn(name="id_annonce")
 	private Annonce annonce1;
 	
 	@Column(nullable = false, updatable = false)
     @Temporal(TemporalType.TIMESTAMP)
     @CreatedDate
     private Date createdAt1;

     @Column(nullable = false)
     @Temporal(TemporalType.TIMESTAMP)
     @LastModifiedDate
     private Date updatedAt1;
     
     
     /* Getters et Setters */
 	////////////////////////////////////////////////////////////
 	
 	public long getId() {
 		return id;
 	}

 	public void setId(long id) {
 		this.id = id;
 	}

 	public String getTitle() {
 		return title;
 	}

 	public void setTitle(String title) {
 		this.title = title;
 	}

 	public String getContent() {
 		return content;
 	}

 	public void setContent(String content) {
 		this.content = content;
 	}

 	public Date getCreatedAt() {
 		return createdAt1;
 	}

 	public void setCreatedAt(Date createdAt) {
 		this.createdAt1 = createdAt;
 	}

 	public Date getUpdatedAt() {
 		return updatedAt1;
 	}

 	public void setUpdatedAt(Date updatedAt) {
 		this.updatedAt1 = updatedAt;
 	}


 	@Column(nullable = false, updatable = false)
     @Temporal(TemporalType.TIMESTAMP)
     @CreatedDate
     private Date createdAt;

     @Column(nullable = false)
     @Temporal(TemporalType.TIMESTAMP)
     @LastModifiedDate
     private Date updatedAt;
 	
     public User getUser() {
 		return user1;
 	}

 	public void setUser(User user) {
 		this.user1 = user;
 	}

 	public Annonce getAnnonce() {
 		return annonce1;
 	}

 	public void setAnnonce(Annonce annonce) {
 		this.annonce1 = annonce;
 	}


 	@ManyToOne
 	@JoinColumn(name="user_id")
 	private User user;
     
 	@ManyToOne
 	@JoinColumn(name="annonce_id")
 	private Annonce annonce;
 	

 }
