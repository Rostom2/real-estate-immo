package com.example.demo.model;

public class Payload {

	private boolean success;
	
	private String log;
	
	private Object objet;

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public Object getObjet() {
		return objet;
	}

	public void setObjet(Object objet) {
		this.objet = objet;
	}

	public Payload() {
		this.success = false;
		this.log = null;
		this.objet = null;
	}
	
	public Payload(boolean success,String log,Object objet) {
		this.success = success;
		this.log = log;
		this.objet = objet;
	}
}
