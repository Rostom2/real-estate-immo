package com.example.demo.model;


import java.util.Date;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="annonces")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
allowGetters = true)

public class Annonce {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;


	@NotBlank
	private int budget;

	@NotBlank
	private long surface;

	@NotBlank
	private int pieces;

	@NotBlank
	private String type;//Chateau, tente, maison...
	
	@NotBlank
	private String location;//location ou vente


	@NotBlank
	private String localisation;


	@NotBlank
	private boolean meubler;

	@NotBlank
	private String titre;

	@NotBlank
	private String contenu;
	
	private int note;
	


	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;	
	
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="annonce")
	private Collection<Comment> comments;


	private String img;
	/*
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="annonce")
	private Collection<Photo_annonces> photos;*/
	
	
	
	/* Getters et Setters */
	////////////////////////////////////////////////////////////

	public long getNote() {
		return note;
	}

	public void setNote(long note) {

		this.note = (int) note;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBudget() {
		return budget;
	}

	public void setBudget(long budget) {
		this.budget = (@NotBlank int) budget;
	}

	public long getSurface() {
		return surface;
	}

	public void setSurface(long surface) {
		this.surface = surface;
	}


	

	public long getPieces() {
		return pieces;
	}

	public void setPieces(long pieces) {

		this.pieces = (@NotBlank int) pieces;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}


	

	public boolean getMeubler() {

		return meubler;
	}

	public void setMeubler(boolean meubler) {
		this.meubler = meubler;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

	public String getImg() {
		return getImg();
	}

	public void setImg(String img) {
		this.img = img;
	}

	

}
