package com.example.demo.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Message;
import com.example.demo.model.User;

public interface MessageRepository extends JpaRepository<Message, Long>{
	
	@Query("SELECT m FROM Message m WHERE m.dest = :user")
	public List<Message> findByDest(@Param("user")User user);
	
	@Query("SELECT m FROM Message m WHERE m.exp = :user")
	public List<Message> findByExp(@Param("user")User user);
	
	@Query("SELECT m FROM Message m WHERE m.date = :date")
	public List<Message> findByDate(@Param("date")Date date);
}
