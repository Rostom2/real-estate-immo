package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	@Query("SELECT u FROM User u WHERE u.login = :login AND u.password = :password")
	public User findByUser(@Param("login") String login, @Param("password") String password);
	
	@Query("SELECT u FROM User u WHERE u.login = :login AND u.password = :password")
	public User findByAdmin(@Param("login") String login, @Param("password") String password);
	
	@Query("SELECT u FROM User u WHERE u.login = :login AND u.email = :email")
	public User findBylogmdp(@Param("login") String login, @Param("email") String email);
	
	@Query("SELECT u FROM User u WHERE u.email = :email")
	public User findByMail(@Param("email") String email);
	
	@Query("SELECT u FROM User u WHERE u.email = :email")
	public List<User> findByAdmin(@Param("email") String email);

	
	
}