package com.example.demo.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Payload;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserRepository userRepo;
	
	@PostMapping("/register")
	public Payload register(@Valid @RequestBody User user) {
		System.out.println(user.toString());
		System.out.println(user.getPassword());
		user.setPassword(MD5(user.getPassword()));
		System.out.println(MD5(user.getPassword()));
		
		user = userRepo.save(user);		
		return new Payload (true, "Vous êtes inscrit", user);
	}
	
	@PostMapping("/signin")
	public Payload signin(@Valid @RequestBody User user, HttpSession session) {
		User userCo = (User)session.getAttribute("user");
		if (userCo==null) {
			user.setPassword(MD5(user.getPassword()));
			User userRes = userRepo.findByUser(user.getLogin(), user.getPassword());
			
			if (userRes != null && userRes.getLogin() != null && !userRes.getLogin().equals("")) {
				session.setAttribute("user", userRes);
				return new Payload (true, "Vous êtes connecté", user);
			}
			return new Payload (false, "Identifiant ou mot de passe incorrect!", user);
		}
		return new Payload (false, "Vous êtes déja connecté en tant que "+userCo.getLogin(), userCo);
	}
	
	@GetMapping("/signout") //plante si déja déco
	public Payload signout(HttpSession session) {
		
		try {
		User newuser = (User)session.getAttribute("user");
		
			if (newuser.getLogin() != null) {
				session.removeAttribute("user");
				return new Payload (true, "Vous avez été déconnecté", newuser);
			}
		}
		catch (Exception e) {
			
		}
		
		return new Payload (false, "Vous êtes déjà déconnecté", "");
		
	}
	
	@GetMapping("/me")
	public Payload infosaboutme(HttpSession session) {
		User newuser = (User)session.getAttribute("user");
		
		if (newuser != null && newuser.getLogin() != null && !newuser.getLogin().equals("")) {
			return new Payload (true,"Bonjour "+newuser.getPrenom()+" "+
					newuser.getNom()+" alias "+
					newuser.getLogin()+" !!!.\n Votre id est "+
					newuser.getId()+". Votre avatar est "+
					newuser.getAvatar()+". Votre adresse email est "+
					newuser.getEmail()+". Votre est status est "+
					newuser.getStatus()+".",newuser);
		}
		return new Payload (false, "Vous n'êtes pas connecté", "");
	}
	
	@PutMapping("/modify")
	public Payload modify(@Valid @RequestBody User userdet, HttpSession session) {
		
		User user = (User)session.getAttribute("user");
		
		if (user != null && user.getLogin() != null && !user.getLogin().equals("")) {
			user.setPrenom(userdet.getPrenom());
			user.setNom(userdet.getNom());
			user.setLogin(userdet.getLogin());
			//user.setPassword(MD5(userdet.getPassword()));
			//user.setAvatar(userdet.getAvatar());
			user.setEmail(userdet.getEmail());
			
			User updatedUser = userRepo.save(user);
			
			return new Payload (true, "Informations enregistrées", updatedUser);
		}
		return new Payload (false, "Vous n'êtes pas connecté", "");
	}
	
	@PutMapping("/forgot")
	public Payload forgot(@Valid @RequestBody User user, HttpSession session) {
		
		User newuser = userRepo.findBylogmdp(user.getLogin(), user.getEmail());
		
		if (newuser != null && newuser.getLogin() != null && !newuser.getLogin().equals("")) {
			
			int table [] = new int [7];
			String str = null;
		
			for (int i = 0; i<table.length; i++) {
				table[i] = (int) (Math.random() * 10);
				str = str + table[i];
			}
			newuser.setPassword(MD5(str));
			
			User updateduser = userRepo.save(newuser);
			return new Payload(true, "Votre nouveau mot de passe est "+str, updateduser);
		}
		return new Payload(false, "Ce login est cet email ne correspondent pas !", "");
	}
	
	/***********************************Seulement pour les admins*****************************************/
	
	@PostMapping("/signinadmin")
	public Payload signinadmin(@Valid @RequestBody User user, HttpSession session) {
		user.setPassword(MD5(user.getPassword()));
		User userRes=null;
		try {
			userRes = userRepo.findByUser(user.getLogin(), user.getPassword());
		}catch (RuntimeException e) {
			e.printStackTrace();
			return new Payload(false,e.getMessage(),user);
		}
		
		if (userRes.getStatus()==1) {
			session.setAttribute("user", userRes);
			return new Payload (true, "Vous êtes connecté", user);
		}
		return new Payload (false,"pas connecté",userRes);
	}
	
	@DeleteMapping("/delete/{id}")
	public Payload delete(@PathVariable(value="id") Long userid) {
		
		User user = userRepo.findById(userid)
				.orElseThrow(() -> new ResourceNotFoundException
						("Note", "id", userid));
		
		userRepo.delete(user);
		
		return new Payload(true, "Suppression réussie", "");
	}
	
	public static String MD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0"+hashtext;
			}
			return hashtext;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


@GetMapping("/searchAllUser")
public Payload searchAllUser() {
	try {
		List<User> users = userRepo.findAll();
		return new Payload(true,"Utilisateur(s) trouvé(s)",users);
	}catch(Exception e) {
		return new Payload(false,e.getMessage(),null);
	}
}


//@PostMapping("/searchUser")
//public Payload searchUser(@Valid @RequestBody User user) {
//	try {
//		String login = user.getLogin();
//		String mail = user.getEmail();
//		List<User> users = userRepo.findByUser();
//		return new Payload(true,"Utilisateur(s) trouvé(s)",users);
//	}catch(Exception e) {
//		return new Payload(false,e.getMessage(),null);
//	}
//}


}