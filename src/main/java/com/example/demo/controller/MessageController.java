package com.example.demo.controller;

import java.sql.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Message;
import com.example.demo.model.Payload;
import com.example.demo.model.User;
import com.example.demo.repository.MessageRepository;
import com.example.demo.repository.UserRepository;


@RestController
@RequestMapping("/api/message")
public class MessageController {
	
	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	
	@GetMapping("/inbox")
	public Payload viewAllFromUser(HttpSession session) {
		User user = (User)session.getAttribute("user");
		if(user==null) {
			return new Payload(false,"Vous devez vous identifier!",null);
		}
		try {
			List<Message> messages = messageRepository.findByDest(user);
			return new Payload(true,"Messages trouvés",messages);
		}catch(RuntimeException e) {
			return new Payload(false,e.getMessage(),null);
		}
	}
	
	@PostMapping("/send")
	public Payload send(@Valid @RequestBody Message message,HttpSession session) {
		User user = (User)session.getAttribute("user");
		System.out.println("title = "+message.getTitle());
		System.out.println("content = "+message.getContent());
		if(user==null) {
			return new Payload(false,"Vous devez vous identifier!",null);
		}
		try {
			User dest = userRepository.findByMail(message.getDest().getEmail());
			System.out.println(dest);
			message.setDest(dest);
			message.setExp(user);
			message.setDate(new Date(0));
			return new Payload(true,"Message envoyé",messageRepository.save(message));
		}catch(RuntimeException e) {		
			return new Payload(false,e.getMessage(),null);
		}
	}

}
