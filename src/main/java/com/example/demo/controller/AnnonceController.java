package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Annonce;
import com.example.demo.model.Payload;
import com.example.demo.model.User;
import com.example.demo.repository.AnnonceRepository;

@RestController
@RequestMapping("/api/annonce")
public class AnnonceController {

	@Autowired
	private AnnonceRepository annonceRepository;
	
	@GetMapping("/searchAll")
	public Payload searchAllAnnonce() {
		try {
			List<Annonce> annonces = annonceRepository.findAll();
			return new Payload(true,"Annonces trouvées",annonces);
		}catch(Exception e) {
			return new Payload(false,e.getMessage(),null);
		}
	}
	
	
	
	@GetMapping("/{id_annonce}")
	public Payload viewAnnonce(@PathVariable("id_annonce") Long idAnnonce) {
		try {
			Annonce annonce = annonceRepository.findById(idAnnonce).orElseThrow(() -> new ResourceNotFoundException("Annonce","id",idAnnonce));
			return new Payload(true,"Annonce trouvée",annonce);
		}catch(Exception e) {
			return new Payload(false,e.getMessage(),null);
		}
	}
	
	
	
	@PostMapping("/create")
	public Payload createAnnonce(@Valid @RequestBody Annonce annonce, HttpSession session) {
		
			return new Payload(true,"Annonce créée", annonceRepository.save(annonce));
		

	}
	
	@PostMapping("/createlistannonces")
	public Payload createlistannonces(@Valid @RequestBody List<Annonce> listannonce, HttpSession session) {
		User user = (User)session.getAttribute("user");
		if (user==null) {
			return new Payload(false,"Veuillez vous identifier pour publier les annonce",listannonce); 
		}
		try {
			((Annonce) listannonce).setUser(user);
			annonceRepository.saveAll(listannonce);
			return new Payload(true,"Annonces créées", "");
		}catch(Exception e) {
			return new Payload(false,e.getMessage(),null);
		}
	}
	
	
	@DeleteMapping("/delete/{id_annonce}")
	public Payload deleteAnnonce(@PathVariable("id_annonce") Long idAnnonce, HttpSession session) {
		User user = (User)session.getAttribute("user");
		Annonce annonce = null;
		
		try {
			annonce = annonceRepository.findById(idAnnonce).orElseThrow(() -> new ResourceNotFoundException("Annonce","id",idAnnonce));
		}catch (Exception e) {
			return new Payload(false,"Annonce non existante",annonce);
		}
		
		if (user==null) {
			return new Payload(false,"Veuillez vous identifier pour supprimer une annonce",annonce); 
		}else if (user.getId()!=annonce.getUser().getId()&&user.getStatus()!=1) {
			return new Payload(false,"Vous ne disposez pas des droits nécessaires",annonce);
		}
		
		try {			
			annonceRepository.delete(annonce);
			return new Payload(true,"Annonce effacée",annonce);
		}catch(Exception e) {
			return new Payload(false,e.getMessage(),annonce);
		}
	}
	
	
	@GetMapping("/{id_annonce}/contact")
	public Payload viewUserContact(@PathVariable("id_annonce") Long idAnnonce) {
		try {
			Annonce annonce = annonceRepository.findById(idAnnonce).orElseThrow(() -> new ResourceNotFoundException("Annonce","id",idAnnonce));
			return new Payload(true,"Utilisateur trouvé",annonce.getUser());
		}catch(Exception e) {
			return new Payload(false,e.getMessage(),null);
		}
	}
	
	
	@PutMapping("/modify/{id_annonce}")
	public Payload modify(@PathVariable(value="id_annonce") Long idAnnonce, @Valid @RequestBody Annonce annonceReq, HttpSession session) {
		User user = (User)session.getAttribute("user");
		try {
			Annonce annonce = annonceRepository.findById(idAnnonce)
					.orElseThrow(() -> new ResourceNotFoundException
							("Annonce", "id", idAnnonce));
			String log = "Vous ne disposez pas des droits pour modifier cette annonce";
			if(user!=null && user.getId()==annonce.getUser().getId()) {
				annonce.setBudget(annonceReq.getBudget());
				annonce.setContenu(annonceReq.getContenu());
				annonce.setLocalisation(annonceReq.getLocalisation());
				annonce.setMeubler(annonceReq.getMeubler());
				annonce.setNote(annonceReq.getNote());
				annonce.setPieces(annonceReq.getPieces());
				annonce.setSurface(annonceReq.getSurface());
				annonce.setTitre(annonceReq.getTitre());
				annonce.setType(annonceReq.getType());
				annonce.setUpdatedAt(new Date());
				
				annonceRepository.save(annonce);
				log = "Informations enregistrées";
			}
			return new Payload (true, log, annonce);
		}catch(Exception e) {
			return new Payload (false, e.getMessage(), annonceReq);
		}
		
	}

}
