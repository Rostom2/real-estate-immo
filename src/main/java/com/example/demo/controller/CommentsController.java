package com.example.demo.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Annonce;
import com.example.demo.model.Comment;
import com.example.demo.model.Payload;
import com.example.demo.model.User;
import com.example.demo.repository.CommentRepository;

@RestController
@RequestMapping("/api/annonce/{id_annonce}")
public class CommentsController {

	@Autowired
	CommentRepository commentRepository;


	@GetMapping("/Comments")
	public Payload getAllComments(@PathVariable(value="id_annonce") Long annonceId) {


		return new Payload(true,"Commentaires trouvés",commentRepository.findAll());


	}

	@PostMapping("/write")
	public Payload write(@PathVariable(value="id_annonce") Long annonceID,@Valid@RequestBody Comment comment,HttpSession session) {

		User user = (User)session.getAttribute("user");

		if (user != null && user.getLogin() != null && !user.getLogin().equals("")) {
			
			comment.setUser(user);
			Annonce annonce = new Annonce();
			annonce.setId(annonceID);
			comment.setAnnonce(annonce);

			return new Payload(true,"Commentaires crée",commentRepository.save(comment));

		}

		return new Payload(false,"Vous devez être connecté",null);
	}

	@DeleteMapping("/delete/{id}")
	public Payload delete(@PathVariable(value="id") Long id,HttpSession session) {

		User user = (User)session.getAttribute("user");

		if (user != null && user.getLogin() != null && !user.getLogin().equals("")) {

			Comment com = commentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));
			commentRepository.delete(com);

			return new Payload(true,"Commentaires détruit",com);
		}
		return new Payload(false,"vous devez être conncté",null);
	}

	@PutMapping("/modify/{id}")
	public Payload modify(@PathVariable(value="id") Long id,@Valid@RequestBody Comment comment,HttpSession session) {

		User user = (User)session.getAttribute("user");

		if (user != null && user.getLogin() != null && !user.getLogin().equals("")) {

			Comment com = commentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));


			com.setTitle(comment.getTitle());
			com.setContent(comment.getContent());

			return new Payload(true,"Modification effectuée",commentRepository.save(com));
		}
		return new Payload(false,"vous devez être conncté",null);
	}


}
